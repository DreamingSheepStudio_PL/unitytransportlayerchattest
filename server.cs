using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using UnityEngine.UI;
using System;
using UnityEditor;
using System.IO;

public class ServerClient
{
    public int connectionId;
    public string playerName;
    public string IpAdress;
}


public class Server : MonoBehaviour {

    private const int MAX_CONNECTION = 100;

    private int port = 16025;

    private int hostId;
    private int webHostId;

    private int reliableChannel;
    private int unreliableChannel;

    private bool isStarted = false;
    private byte error;

    public InputField PortSetInput;
    public InputField ChatDataInput;

    public Text DebugText;
    public Text ChatText;

    public Transform SettingUpPanel;
    public Transform ChatPanel;

    private float RunTime=0;

    bool PortSet = false;

    private List<ServerClient> clients = new List<ServerClient>();

    private string ChatHistory;

    int logLinesWritten =0;

    private void Start()
    {
        FillServerLogs();
        PortSetInput.Select();
    }
    public void SetPort()
    {
        string por = PortSetInput.text;
        if (por == "")
        {
            return;
        }
        port = Convert.ToInt32(por);
        PortSet = true;
    }

    private void StartServer()
    {
        NetworkTransport.Init();
        ConnectionConfig cc = new ConnectionConfig();

        reliableChannel = cc.AddChannel(QosType.Reliable);
        unreliableChannel = cc.AddChannel(QosType.Unreliable);

        HostTopology topo = new HostTopology(cc, MAX_CONNECTION);

        hostId = NetworkTransport.AddHost(topo, port, null);
        webHostId = NetworkTransport.AddWebsocketHost(topo, port, null);

        DebugText.text += "\n" + RunTime + " SERVER RUNNING: Server Port: " + port;
        SettingUpPanel.gameObject.SetActive(false);
        ChatPanel.gameObject.SetActive(true);
        ChatDataInput.Select();

        isStarted = true;
    }

    private void Update()
    {
        RunTime = Time.time;
        FillServerLogs();
        if (PortSet == true)
        {
            if (!isStarted)
            {
                StartServer();
                return;
            }
            int recHostId;
            int connectionId;
            int channelId;
            byte[] recBuffer = new byte[1024];
            int bufferSize = 1024;
            int dataSize;
            byte error;
            NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
            switch (recData)
            {
                case NetworkEventType.ConnectEvent:
                    DebugText.text += "\n" + RunTime+" Player " + connectionId + " has connected";
                    OnConnection(connectionId);
                    OnConnectionInfo(recHostId, connectionId);
                    SendChatHistory(connectionId);
                    break;
                case NetworkEventType.DataEvent:
                    string msg = Encoding.Unicode.GetString(recBuffer, 0, dataSize);
                    DebugText.text += ("\n" + RunTime + " Player " + connectionId + " sent " + msg);
                    string[] splitData = msg.Split('|');

                    switch (splitData[0])
                    {
                        case "NAMEIS":
                            OnNameIs(connectionId,splitData[1]);
                            break;

                        case "CNN":
                            break;

                        case "DC":
                            break;
                        case "REQ":
                            OnRequest(connectionId, splitData[2]);
                            break;
                        case "MSG":
                            SendAllChat(connectionId, splitData[2]);
                            break;

                        default:
                            Debug.Log("Invalid message : " + msg);
                            DebugText.text = "\n" + RunTime + " Invalid message : " + msg;
                            break;
                    }

                    break;
                case NetworkEventType.DisconnectEvent:
                    DebugText.text += "\n" + RunTime + " Player " + connectionId + " has disconnected";
                    OnDisconnection(connectionId);
                    break;
                case NetworkEventType.BroadcastEvent:
                    break;
            }
        }
    }
    private void OnConnectionInfo(int recHostId,int cnnId)
    {
        int port;
        string address;
        UnityEngine.Networking.Types.NetworkID netId;
        UnityEngine.Networking.Types.NodeID nodeId;
        NetworkTransport.GetConnectionInfo(recHostId, cnnId, out address, out port, out netId, out nodeId, out error);
        clients.Find(x => x.connectionId == cnnId).IpAdress = address;
        DebugText.text += "\n" + RunTime + " IP: Player" + cnnId +" " + address;
    }
    private void SendChatHistory(int cnnId)
    {
        string msg = "HIS|" + ChatHistory;
        Send(msg, reliableChannel, cnnId);
    }
    private void OnRequest(int cnnId, string chatdata)
    {
        if(chatdata == "/List")
        {
            string msg = "LIST|OnlineList:|";
            foreach (ServerClient sc in clients)
            {
                msg += sc.playerName + "|";
            }
            Send(msg, reliableChannel, cnnId);
        }
    }
    private void OnDisconnection(int cnnId)
    {
        ServerClient c = new ServerClient();

        int i = 0;
        foreach (ServerClient sc in clients)
        {
            if(sc.connectionId == cnnId)
            {
                c = clients[i];
            }
            i++;
        }
        if (c != null)
        {
            string msg = "DC|" + c.playerName + "|" + c.connectionId;
            ChatText.text += "\n" + c.playerName + " has disconnected";
            ChatHistory += "\n" + c.playerName + " has disconnected";
            clients.RemoveAt(i);
            Send(msg, reliableChannel, clients);
        }
    }
    private void OnConnection(int cnnId)
    {
        ServerClient c = new ServerClient();
        c.connectionId = cnnId;
        c.playerName = "TEMP";
        clients.Add(c);

        string msg = "ASKNAME|" + cnnId + "|";
        foreach(ServerClient sc in clients)
        {
            msg += sc.playerName + '%' + sc.connectionId + '|';
        }
        msg = msg.Trim('|');

        Send(msg, reliableChannel, cnnId); 
        
    }
    public void SendItsChat()
    {
        string chatdata = ChatDataInput.text;
        if (chatdata != "")
        {
            if (chatdata[0] != '/')
            {
                string dataToSend = "MSGSRV|" + "SERVER|" + chatdata;
                string dataToShow = "\nYou: " + chatdata;
                ChatHistory += "\nSERVER: " + chatdata;
                ChatText.text += dataToShow;
                Send(dataToSend, reliableChannel, clients);
            }
            else if (chatdata[0] == '/')
            {
                switch (chatdata)
                {
                    case "/List":
                        string chat = "\nPlayer List:";
                        string debug = "\n" + RunTime + " /List:";
                        foreach (ServerClient sc in clients)
                        {
                            chat += "\n" + sc.IpAdress + " " + sc.playerName;
                            debug += "\n" + sc.IpAdress + " " + sc.playerName;
                        }
                        ChatText.text += chat;
                        DebugText.text += debug;

                        break;
                    default:
                        break;
                }
            }
        }
        ChatDataInput.Select();
        ChatDataInput.text = "";
        ChatDataInput.Select();
    }
    private void SendAllChat(int cnnId,string chatdata)
    {
        //Link the name to the connection Id
        string playerName = clients.Find(x => x.connectionId == cnnId).playerName;
        string dataToShow = "\n" + playerName+": " + chatdata;
        ChatHistory += dataToShow;
        ChatText.text += dataToShow;

        //Tell everybody that a new player has connected
        Send("MSGSRV|" + playerName + '|' + chatdata, reliableChannel, clients);
    }

    private void OnNameIs(int cnnId,string playerName)
    {
        //Link the name to the connection Id
        clients.Find(x => x.connectionId == cnnId).playerName = playerName;

        string dataToShow = "\n" + playerName + ": has connected";
        ChatHistory += dataToShow;
        ChatText.text += dataToShow;

        //Tell everybody that a new player has connected
        Send("CNN|" + playerName + '|' + cnnId, reliableChannel, clients);
    }

    private void Send(string message, int channelId, int cnnId)
    {
        List<ServerClient> c = new List<ServerClient>();
        c.Add(clients.Find(x => x.connectionId == cnnId));
        Send(message, channelId, c);
    }
    private void Send(string message, int channelId,List<ServerClient> c)
    {
        DebugText.text += "\n" + RunTime + " Sending : " + message;
        byte[] msg = Encoding.Unicode.GetBytes(message);
        foreach (ServerClient sc in c)
        {
            NetworkTransport.Send(hostId, sc.connectionId, channelId, msg, message.Length * sizeof(char), out error);
        }
    }
    private void FillServerLogs()
    {
        string subPath = "LOGS";
        bool exists = Directory.Exists(subPath);

        if (!exists)
            Directory.CreateDirectory(subPath);

        string path = subPath + "/LOG.txt";
        if (!File.Exists(path))
        {
            File.Create(path);
        }
        else if (File.Exists(path))
        {
            string[] debugtext = DebugText.text.Split('\n');
            if(logLinesWritten< debugtext.Length)
            {
                StreamWriter tw = new StreamWriter(path,true);
                int k = logLinesWritten;
                for (int i = k;i<debugtext.Length;i++)
                {
                    tw.WriteLine(debugtext[i]+"\n");
                    logLinesWritten++;
                }
                tw.Close();
            }
        }
    }

}


