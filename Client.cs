using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Client : MonoBehaviour {

    private const int MAX_CONNECTION = 100;

    private int port = 16025;

    private int hostId;
    private int webHostId;

    private int ourClientId;
    private int connectionId;

    private int reliableChannel;
    private int unreliableChannel;

    private float ConnectionTime;
    private bool isConnected = false;
    private bool isStarted = false;
    private byte error;


    public Transform ConnectionPanel;
    public Transform ChatPanel;

    public InputField InputIPText;
    public InputField InputNameText;
    public InputField InputChatText;

    public Text DebugText;
    public Text ChatText;

    string playerName;
    string Ip;
    private void Start()
    {
        InputIPText.Select();
    }
    public void Connect()
    {
        string pName = InputNameText.text;
        string ip = InputIPText.text;
        if (ip == "" || !ip.Contains(":"))
        {
            return;
        }
        string[] ipdata = ip.Split(':');
        Ip = ipdata[0];
        port = Convert.ToInt32(ipdata[1]);

        if (pName == "")
        {
            DebugText.text += "Type the name to connect";

            return;
        }

        playerName = pName;

        NetworkTransport.Init();
        ConnectionConfig cc = new ConnectionConfig();

        reliableChannel = cc.AddChannel(QosType.Reliable);
        unreliableChannel = cc.AddChannel(QosType.Unreliable);

        HostTopology topo = new HostTopology(cc, MAX_CONNECTION);

        hostId = NetworkTransport.AddHost(topo, 0);
        connectionId = NetworkTransport.Connect(hostId, Ip, port, 0, out error);

        ConnectionTime = Time.time;
        DebugText.text += "\nTRYING TO CONNTECT TO: " + ip;

        ConnectionPanel.gameObject.SetActive(false);
        ChatPanel.gameObject.SetActive(true);
        InputChatText.Select();

        isConnected = true;
    }

    private void Update()
    {
        if (isConnected == false)
            return;

        int recHostId;
        int connectionId;
        int channelId;
        byte[] recBuffer = new byte[1024];
        int bufferSize = 1024;
        int dataSize;
        byte error;
        NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
        switch (recData)
        {
            case NetworkEventType.DataEvent:
                string msg = Encoding.Unicode.GetString(recBuffer, 0, dataSize);
                DebugText.text += "\nReceiving : " + msg;
                string[] splitData = msg.Split('|');

                switch(splitData[0])
                {
                    case "ASKNAME":
                        OnAskName(splitData);
                        break;

                    case "CNN":
                        OnCNN(splitData);
                        break;
                    case "HIS":
                        OnHistory(splitData);
                        break;
                    case "LIST":
                        OnList(splitData);
                        break;
                    case "DC":
                        OnDisconnection(splitData);
                        break;
                    case "MSGSRV":
                        OnMSG(splitData);
                        break;

                    default:
                        Debug.Log("Invalid message : " + msg);
                        DebugText.text += "\nInvalid message : " + msg;
                        break;

                }

                break;
        }
    }
    private void OnHistory(string[] data)
    {
        ChatText.text += data[1] + "\n ==== HISTORY ==== ";
    }
    private void OnDisconnection(string[] data)
    {
        ChatText.text += "\n" + data[1] + " has disconnected";
    }
    public void SendChat()
    {
        string chatdata = InputChatText.text;
        InputChatText.Select();
        InputChatText.text = "";
        if (chatdata != "")
        {
            char[] chatchar = chatdata.ToCharArray();
            if (chatchar[0] == '/')
            {
                string dataToSend = "REQ|" + playerName + "|" + chatdata;
                Send(dataToSend, reliableChannel);
            }
            else
            {
                string dataToSend = "MSG|" + ourClientId + "|" + chatdata;
                string dataToShow = "\nYou: " + chatdata;
                ChatText.text += dataToShow;
                Send(dataToSend, reliableChannel);
            }
        }
    }
    private void OnList(string[] data)
    {
        for (int i = 1; i<data.Length-1;i++)
        {
            ChatText.text += "\n" + data[i];
        }
    }
    private void OnMSG(string[] data)
    {
        if (data[1] != playerName)
            ChatText.text += "\n" + data[1] + ": "+ data[2];
    }
    private void OnCNN(string[] data)
    {
        ChatText.text += "\n" + data[1] + ": has connected";
    }

    private void OnAskName(string[] data)
    {
        // set this client's ID
        ourClientId = int.Parse(data[1]);
        //Send our name to the server
        Send("NAMEIS|" + playerName, reliableChannel);

        //Create all the other players
        for (int i =2;i< data.Length-1; i++)
        {
            string[] d = data[i].Split('%');
            SpawnPlayer(d[0], int.Parse(d[1]));
        }
    }

    private void SpawnPlayer(string playerName,int cnnId)
    {

    }

    private void Send(string message, int channelId)
    {
        DebugText.text += "\nSending : " + message;
        byte[] msg = Encoding.Unicode.GetBytes(message);
            
        NetworkTransport.Send(hostId, connectionId, channelId, msg, message.Length * sizeof(char), out error);
    }
}
